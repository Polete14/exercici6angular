import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form-entrada',
  templateUrl: './form-entrada.component.html',
  styleUrls: ['./form-entrada.component.css']
})
export class FormEntradaComponent implements OnInit {

  @Output() entrada = new EventEmitter<{anyFabricacio: string, model: string, marca: string}>();

  constructor() { }

  ngOnInit() {
  }

  model = "";
  marca = "";
  anyFabricacio = "";

  afegir(){
    this.entrada.emit({
      anyFabricacio: this.anyFabricacio,
      model: this.model,
      marca: this.marca
    })
  }

}
