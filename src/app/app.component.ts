import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Exercici6';
  dispositiu = {
    "anyFabricacio" : "",
    "model" : "",
    "marca" : ""
  }
  dispositius = [
    {
      "anyFabricacio": "2018", "model": "X", "marca": "Iphone"
    },
  ]
  afegir(entrada: {anyFabricacio: string, model: string, marca: string}){
    this.dispositiu.anyFabricacio = entrada.anyFabricacio;
    this.dispositiu.model = entrada.model;
    this.dispositiu.marca = entrada.marca;
    this.dispositius.push(this.dispositiu);
  }
  eliminar(sortida: {id: number}){
    delete this.dispositius[sortida.id];
  }
}
