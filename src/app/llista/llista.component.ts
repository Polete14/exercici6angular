import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Variable } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-llista',
  templateUrl: './llista.component.html',
  styleUrls: ['./llista.component.css']
})
export class LlistaComponent implements OnInit {

  @Input() dispositiu : object;
  @Output() sortida = new EventEmitter<{id: number}>();

  constructor() { }

  ngOnInit() {
  }
  eliminar(i){
    this.sortida.emit({
      id : i,
    })
  }
}
